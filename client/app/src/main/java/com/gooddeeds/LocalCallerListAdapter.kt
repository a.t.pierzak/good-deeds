package com.gooddeeds

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class LocalCallerListAdapter(private val callerDataDataset: Array<String>, private val localCallerListViewModel: LocalCallerListViewModel) : RecyclerView.Adapter<LocalCallerListAdapter.CallerDataViewHolder>() {

    inner class CallerDataViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(phoneNumber: String) {
            view.findViewById<TextView>(R.id.phoneNumberTextView).apply {
                text = phoneNumber
            }
            view.findViewById<Button>(R.id.confirmHelpButton).apply {
                setOnClickListener { v ->
                    localCallerListViewModel.updateCallerData(phoneNumber)
                    val intent = Intent(v.context, CurrentTaskActivity::class.java)
                    intent.putExtra("phoneNumber", phoneNumber)
                    v.context.startActivity(intent)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): CallerDataViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_caller_data, parent, false)

        return CallerDataViewHolder(view)
    }

    override fun onBindViewHolder(holder: CallerDataViewHolder, position: Int) {
        holder.bind(callerDataDataset[position])
    }

    override fun getItemCount(): Int = callerDataDataset.size
}