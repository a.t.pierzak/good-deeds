package com.gooddeeds

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class MapsViewModel(application: Application) : AndroidViewModel(application) {

    private val apiDataProvider = APIDataProvider(application)

    val mapData: MutableLiveData<List<CallerData>> = MutableLiveData(emptyList())

    fun getCallersData() {
        apiDataProvider.getCallersData({ callerDataList ->
            mapData.postValue(callerDataList)
        }, {
            println("--dbg Volley error: $it")
        })
    }

    fun getCallerDataPhoneNumberArrayForPostcode(postcode: String) : Array<String> {
        val callerDataList = mutableListOf<String>()
        for (callerData in mapData.value!!) {
            if (callerData.postCode == postcode) {
                callerDataList.add(callerData.phoneNumber)
            }
        }
        return callerDataList.toTypedArray()
    }

}