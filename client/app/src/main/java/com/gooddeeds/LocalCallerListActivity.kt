package com.gooddeeds

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class LocalCallerListActivity : AppCompatActivity() {

    private val localCallerListViewModel: LocalCallerListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_local_caller_list)

        findViewById<TextView>(R.id.titleTextView).apply {
            text = String.format(getString(R.string.local_caller_list_text_view),intent.getStringExtra("postcode"))
        }

        val phoneNumberArray = intent.getStringArrayExtra("phoneNumberList")

        val viewManager = LinearLayoutManager(this)
        val viewAdapter = LocalCallerListAdapter(phoneNumberArray!!, localCallerListViewModel)

        findViewById<RecyclerView>(R.id.callerDataRecyclerView).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

}
