package com.gooddeeds

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var mMap: GoogleMap

    private val mapViewModel: MapsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.light_style))

        mapViewModel.mapData.observe(this, Observer { callerDataList ->
            val elderlyPersonIcon = BitmapDescriptorFactory.fromResource(R.drawable.icons8_old_man_64)
            for (callerData in callerDataList) {
                mMap.addMarker(MarkerOptions().position(LatLng(callerData.latitude, callerData.longitude)).title(callerData.postCode).icon(elderlyPersonIcon))
            }
        })
        mapViewModel.getCallersData()
        mMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(50.8840677, 20.5926139)))
        mMap.setOnMarkerClickListener(this)
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        if (marker != null) {
            val intent = Intent(this, LocalCallerListActivity::class.java)
            val callerPhoneNumberArray = mapViewModel.getCallerDataPhoneNumberArrayForPostcode(marker.title)
            intent.putExtra("postcode", marker.title)
            intent.putExtra("phoneNumberList", callerPhoneNumberArray)
            startActivity(intent)
        }
        return true
    }
}
