package com.gooddeeds

import android.os.SystemClock
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*

const val ONE_SECOND: Long = 1000
const val ONE_HOUR = 1000 * 60 * 60

class CurrentTaskViewModel : ViewModel() {

    val elapsedTime = MutableLiveData<Long>()

    private val initialTime = SystemClock.elapsedRealtime() + ONE_HOUR * 2
    private val timer = Timer()

    init {
        timer.scheduleAtFixedRate(
            object : TimerTask() {
                override fun run() {
                    val newValue = (initialTime - SystemClock.elapsedRealtime()) / 1000
                    elapsedTime.postValue(newValue)
                }
            }, ONE_SECOND, ONE_SECOND
        )
    }

    override fun onCleared() {
        super.onCleared()
        timer.cancel()
    }

}