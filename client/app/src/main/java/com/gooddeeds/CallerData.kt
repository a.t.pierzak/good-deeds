package com.gooddeeds

data class CallerData (val phoneNumber: String,
                       val postCode:String,
                       val latitude: Double,
                       val longitude: Double)