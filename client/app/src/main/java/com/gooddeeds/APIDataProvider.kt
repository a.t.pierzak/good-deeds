package com.gooddeeds

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.RetryPolicy
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley

class APIDataProvider(appContext: Context) {

    private val queue = Volley.newRequestQueue(appContext)

    fun getCallersData(successCallback: (List<CallerData>) -> Unit, failureCallback: (VolleyError) -> Unit) {
        val url = "http://good-deeds-server.herokuapp.com/api/v1/resources/tickets/all"
        val jsonArrayRequest = JsonArrayRequest(Request.Method.GET, url, null,
            Response.Listener { response ->
                val callerDataList = mutableListOf<CallerData>()
                var index = 0
                while (!response.isNull(index)) {
                    val jsonObject = response.getJSONObject(index)
                    val phoneNumber = jsonObject.getString("phone_number")
                    val postcode = jsonObject.getString("postcode")
                    val latitude = jsonObject.getString("latitude")
                    val longitude = jsonObject.getString("longitude")
                    callerDataList.add(CallerData(phoneNumber, postcode, latitude.toDouble(), longitude.toDouble()))
                    index++
                }
                successCallback(callerDataList)
            }, Response.ErrorListener { error ->
                failureCallback(error)
            })
        jsonArrayRequest.retryPolicy = object : RetryPolicy {
            override fun getCurrentRetryCount(): Int {
                return 50000
            }

            override fun getCurrentTimeout(): Int {
                return 50000
            }

            override fun retry(error: VolleyError?) {
                println("--dbg Retry called")
            }
        }
        queue.add(jsonArrayRequest)
    }

}