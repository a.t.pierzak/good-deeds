package com.gooddeeds

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.activity.viewModels

class CurrentTaskActivity : AppCompatActivity() {

    private val currentTaskViewModel: CurrentTaskViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_current_task)

        findViewById<TextView>(R.id.phoneTextView).apply {
            text = intent.getStringExtra("phoneNumber")
        }
        val timerTextView = findViewById<TextView>(R.id.timerTextView)
        currentTaskViewModel.elapsedTime.observe(this, Observer { elapsedTime ->
            val hours = elapsedTime / 3600
            val minutes = (elapsedTime % 3600) / 60
            val seconds = (elapsedTime % 3600) % 60
            timerTextView.text = "$hours:$minutes:$seconds"
        })
    }
}
